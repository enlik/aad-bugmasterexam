package com.google.developer.bugmaster;


import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testQuizButtonClick() {
        Espresso.onView(ViewMatchers.withId(R.id.fab)).perform(ViewActions.click());

        Espresso.onView(ViewMatchers.withId(R.id.text_question)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.answer_select)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.text_correct)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }

    @Test
    public void testInsectDisplayContent() {
        for (int i = 0; i < 25; i++) {
            recycleViewClick(i);
            Espresso.onView(ViewMatchers.withId(R.id.item_friendlyname)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
            Espresso.onView(ViewMatchers.withId(R.id.item_scientificname)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
            Espresso.onView(ViewMatchers.withId(R.id.tv_classification)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
            Espresso.pressBack();
        }
    }

    private void recycleViewClick(int position) {
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view)).perform(RecyclerViewActions.scrollToPosition(position));
        Espresso.onView(ViewMatchers.withId(R.id.recycler_view)).
                perform(RecyclerViewActions.actionOnItemAtPosition(position, ViewActions.click()));
    }

}
