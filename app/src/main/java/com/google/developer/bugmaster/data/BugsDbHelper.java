package com.google.developer.bugmaster.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.developer.bugmaster.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Database helper class to facilitate creating and updating
 * the database from the chosen schema.
 */
public class BugsDbHelper extends SQLiteOpenHelper {
    private static final String TAG = BugsDbHelper.class.getSimpleName();

    private static final String DATABASE_NAME = "insects.db";
    private static final int DATABASE_VERSION = 1;

    //Used to read data from res/ and assets/
    private Resources mResources;

    public static final String TABLE_NAME = "insects";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FRIENDLY_NAME = "friendlyName";
    public static final String COLUMN_SCIENTIFIC_NAME = "scientificName";
    public static final String COLUMN_CLASSIFICATION = "classification";
    public static final String COLUMN_IMAGE_ASSET = "imageAsset";
    public static final String COLUMN_DANGER_LEVEL = "dangerLevel";

    private static final String CREATE_DATABASE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
            + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_FRIENDLY_NAME + " VARCHAR NOT NULL, "
            + COLUMN_SCIENTIFIC_NAME + " VARCHAR NOT NULL, "
            + COLUMN_CLASSIFICATION + " VARCHAR NOT NULL, "
            + COLUMN_IMAGE_ASSET + " VARCHAR NOT NULL, "
            + COLUMN_DANGER_LEVEL + " INTEGER NOT NULL);";

    public BugsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mResources = context.getResources();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //TODO: Create and fill the database
        db.execSQL(CREATE_DATABASE);

        try {
            readInsectsFromResources(db);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: Handle database version upgrades
        db.execSQL("DROP TABLE IF EXIST " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Streams the JSON data from insect.json, parses it, and inserts it into the
     * provided {@link SQLiteDatabase}.
     *
     * @param db Database where objects should be inserted.
     * @throws IOException
     * @throws JSONException
     */
    private void readInsectsFromResources(SQLiteDatabase db) throws IOException, JSONException {
        StringBuilder builder = new StringBuilder();
        InputStream in = mResources.openRawResource(R.raw.insects);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        //Parse resource into key/values
        final String rawJson = builder.toString();
        //TODO: Parse JSON data and insert into the provided database instance
		JSONArray data = new JSONObject(rawJson).getJSONArray(TABLE_NAME);
        for (int i = 0; i < data.length(); i++) {
            JSONObject item = data.getJSONObject(i);

            Log.d(TAG, "readInsectsFromResources | COLUMN_FRIENDLY_NAME: " + item.get(COLUMN_FRIENDLY_NAME));

            ContentValues values = new ContentValues();
            values.put(COLUMN_FRIENDLY_NAME, item.getString(COLUMN_FRIENDLY_NAME));
            values.put(COLUMN_SCIENTIFIC_NAME, item.getString(COLUMN_SCIENTIFIC_NAME));
            values.put(COLUMN_CLASSIFICATION, item.getString(COLUMN_CLASSIFICATION));
            values.put(COLUMN_IMAGE_ASSET, item.getString(COLUMN_IMAGE_ASSET));
            values.put(COLUMN_DANGER_LEVEL, item.getInt(COLUMN_DANGER_LEVEL));

            db.replace(TABLE_NAME, null, values);
        }
    }
}
