package com.google.developer.bugmaster.reminders;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.developer.bugmaster.MainActivity;
import com.google.developer.bugmaster.QuizActivity;
import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.data.DatabaseManager;
import com.google.developer.bugmaster.data.Insect;

import java.util.ArrayList;
import java.util.Random;

public class ReminderService extends IntentService {

    private static final String TAG = ReminderService.class.getSimpleName();

    private static final int NOTIFICATION_ID = 42;

    public ReminderService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Quiz reminder event triggered");

        //Present a notification to the user
        NotificationManager manager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Create action intent
        Intent action = actionIntentHandler();
        //TODO: Add data elements to quiz launch

        //Create back intent
        Intent backIntent = new Intent(this, MainActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent operation = PendingIntent.getActivities(this, 0, new Intent[]{backIntent, action}, PendingIntent.FLAG_ONE_SHOT);

        Notification note = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_text))
                .setSmallIcon(R.drawable.ic_bug_empty)
                .setContentIntent(operation)
                .setAutoCancel(true)
                .build();

        manager.notify(NOTIFICATION_ID, note);
    }

    private Intent actionIntentHandler() {
        DatabaseManager db = DatabaseManager.getInstance(this);
        ArrayList<Insect> randoms = new ArrayList<>();
        Random random = new Random();
        Cursor insectCursor = db.queryAllInsects(DatabaseManager.SORT_BY_NAME);

        for (int i = 0; i < QuizActivity.ANSWER_COUNT; i++) {
            if (insectCursor.moveToPosition(random.nextInt(insectCursor.getCount() - 1))) {
                randoms.add(new Insect(insectCursor));
            }
        }

        Intent intent = new Intent(this, QuizActivity.class);
        intent.putParcelableArrayListExtra(QuizActivity.EXTRA_INSECTS, randoms);
        intent.putExtra(QuizActivity.EXTRA_ANSWER, randoms.get(random.nextInt(QuizActivity.ANSWER_COUNT - 1)));

        return intent;
    }
}
