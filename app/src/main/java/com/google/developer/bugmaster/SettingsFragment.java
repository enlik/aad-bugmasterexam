package com.google.developer.bugmaster;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;

import com.google.developer.bugmaster.reminders.AlarmReceiver;

public class SettingsFragment extends PreferenceFragment implements SwitchPreference.OnPreferenceChangeListener {

    private SharedPreferences preferences;

    private SwitchPreference reminder;
    private ListPreference alarm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        reminder = (SwitchPreference) findPreference(getString(R.string.pref_key_reminder));
        reminder.setOnPreferenceChangeListener(this);

        alarm = (ListPreference) findPreference(getString(R.string.pref_key_alarm));
        alarm.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        if (preference.getKey().equals(getString(R.string.pref_key_reminder))) {
            preferences.edit().putBoolean(getString(R.string.pref_key_reminder), !reminder.isChecked()).apply();
            AlarmReceiver.scheduleAlarm(getActivity());
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    AlarmReceiver.scheduleAlarm(getActivity());
                }
            }, 1000);
        }
        return true;
    }
}
