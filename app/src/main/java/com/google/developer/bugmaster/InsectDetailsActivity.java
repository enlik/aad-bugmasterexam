package com.google.developer.bugmaster;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.developer.bugmaster.data.Insect;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InsectDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_DATA = "data";

    @BindView(R.id.iv_insect)
    ImageView iv_insect;

    @BindView(R.id.item_friendlyname)
    TextView item_friendlyname;

    @BindView(R.id.item_scientificname)
    TextView item_scientificname;

    @BindView(R.id.tv_classification)
    TextView tv_classification;

    @BindView(R.id.rb_danger_level)
    RatingBar rb_danger_level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: Implement layout and display insect details
		setContentView(R.layout.activity_insect_details);

        ButterKnife.bind(this);

        Insect insect = getIntent().getParcelableExtra(EXTRA_DATA);

        try {
            InputStream ims = getAssets().open(insect.imageAsset);
            Drawable d = Drawable.createFromStream(ims, null);
            iv_insect.setImageDrawable(d);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        item_friendlyname.setText(insect.name);
        item_scientificname.setText(insect.scientificName);
        tv_classification.setText(getString(R.string.classification, insect.classification));
        rb_danger_level.setRating(insect.dangerLevel);
    }
}
