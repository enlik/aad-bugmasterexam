package com.google.developer.bugmaster;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.developer.bugmaster.data.DatabaseManager;
import com.google.developer.bugmaster.data.Insect;
import com.google.developer.bugmaster.data.InsectRecyclerAdapter;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String EXTRA_SORT = "sort";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private InsectRecyclerAdapter adapter;
    private String sortOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        fab.setOnClickListener(this);

        loadData(DatabaseManager.SORT_BY_NAME);

        if (savedInstanceState != null) {
            String sort = savedInstanceState.getString(EXTRA_SORT);
            loadData(sort);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_SORT, sortOrder);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                //TODO: Implement the sort action
				toggleSort();
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Click events in Floating Action Button */
    @Override
    public void onClick(View v) {
        //TODO: Launch the quiz activity
        ArrayList<Insect> randomInsects = new ArrayList<>();
		Random random = new Random();

        for (int i = 0; i < QuizActivity.ANSWER_COUNT; i++) {
            randomInsects.add(adapter.getItem(random.nextInt(adapter.getItemCount() - 1)));
        }

        Intent intent = new Intent(this, QuizActivity.class);
        intent.putParcelableArrayListExtra(QuizActivity.EXTRA_INSECTS, randomInsects);
        intent.putExtra(QuizActivity.EXTRA_ANSWER, randomInsects.get(random.nextInt(QuizActivity.ANSWER_COUNT - 1)));
        startActivity(intent);
    }

    private void loadData(String sortOrder) {
        this.sortOrder = sortOrder;

        DatabaseManager db = DatabaseManager.getInstance(this);
        Cursor cursor = db.queryAllInsects(sortOrder);

        adapter = new InsectRecyclerAdapter(cursor);

        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        recycler_view.setAdapter(adapter);
    }

    private void toggleSort() {
        if (sortOrder == DatabaseManager.SORT_BY_NAME) {
            loadData(DatabaseManager.SORT_BY_DANGER);
        }

        else {
            loadData(DatabaseManager.SORT_BY_NAME);
        }
    }
}
