package com.google.developer.bugmaster.data;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.developer.bugmaster.InsectDetailsActivity;
import com.google.developer.bugmaster.R;
import com.google.developer.bugmaster.views.DangerLevelView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * RecyclerView adapter extended with project-specific required methods.
 */

public class InsectRecyclerAdapter extends
        RecyclerView.Adapter<InsectRecyclerAdapter.InsectHolder> {

    /* ViewHolder for each insect item */
    public class InsectHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_danger_level)
        DangerLevelView item_danger_level;

        @BindView(R.id.item_friendlyname)
        TextView item_friendlyname;

        @BindView(R.id.item_scientificname)
        TextView item_scientificname;

        public InsectHolder(View itemView) {


            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(final Insect insect) {
            item_danger_level.setDangerLevel(insect.dangerLevel);
            item_friendlyname.setText(insect.name);
            item_scientificname.setText(insect.scientificName);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), InsectDetailsActivity.class);
                    intent.putExtra(InsectDetailsActivity.EXTRA_DATA, insect);
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }

    private Cursor mCursor;

    public InsectRecyclerAdapter(Cursor mCursor) {
        this.mCursor = mCursor;
    }

    @Override
    public InsectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InsectHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.activity_main_item, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(InsectHolder holder, int position) {
        Insect insect = getItem(position);
        holder.bind(insect);
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    /**
     * Return the {@link Insect} represented by this item in the adapter.
     *
     * @param position Adapter item position.
     *
     * @return A new {@link Insect} filled with this position's attributes
     *
     * @throws IllegalArgumentException if position is out of the adapter's bounds.
     */
    public Insect getItem(int position) {
        if (position < 0 || position >= getItemCount()) {
            throw new IllegalArgumentException("Item position is out of adapter's range");
        } else if (mCursor.moveToPosition(position)) {
            return new Insect(mCursor);
        }
        return null;
    }
}
